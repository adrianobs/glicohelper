package com.example.flint.glicohelper.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.flint.glicohelper.R;
import com.example.flint.glicohelper.activities.GlycemiaSevenDays;
import com.example.flint.glicohelper.database.DatabaseHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FragmentNewValue extends Fragment {
    private Button dateValue, timeValue, sendValue, seeDiary;
    private EditText value;
    private TextView lastValue, avgValue;
    private DatabaseHelper helper;
    private SQLiteDatabase db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_value, container, false);

        dateValue = (Button) rootView.findViewById(R.id.button_dateValue);
        timeValue = (Button) rootView.findViewById(R.id.button_timeValue);
        sendValue = (Button) rootView.findViewById(R.id.button_sendValue);
        seeDiary = (Button) rootView.findViewById(R.id.button_seeDiary);
        value = (EditText) rootView.findViewById(R.id.editText_value);
        lastValue = (TextView) rootView.findViewById(R.id.textView_lastValue_text);
        avgValue = (TextView) rootView.findViewById(R.id.textView_avgValue);
        helper =  new DatabaseHelper(getContext());
        value.setText("100");

        changeDate(-1, -1, -1);
        changeTime(-1, -1);
        writeValuesInfo();

        dateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDate(0,0,0);
            }
        });

        timeValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTime(0, 0);
            }
        });

        sendValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendValue();
            }
        });

        seeDiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), GlycemiaSevenDays.class));
            }
        });
        return rootView;
    }

    private void sendValue(){
        db = helper.getReadableDatabase();

        String[] fields = {"sensibility", "glycemiaGoal"};
        Cursor cursor = db.query("user", fields, null, null, null, null, null);
        cursor.moveToFirst();

        int goal = cursor.getInt(cursor.getColumnIndex("glycemiaGoal"));
        int sens = cursor.getInt(cursor.getColumnIndex("sensibility"));
        int glycemia = Integer.parseInt(value.getText().toString());
        final double bolus = (double) (glycemia - goal)/sens;

        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("date", dateValue.getText().toString());
        values.put("time", timeValue.getText().toString());
        values.put("value", glycemia);
        values.put("type", getResources().getString(R.string.database_type_glycemia));

        long result = db.insert("glycemia", null, values);

        if(result != -1){
            Toast.makeText(getContext(), "Glicemia registrada.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Glicemia não registrada.", Toast.LENGTH_SHORT).show();
        }

        if (Integer.parseInt(value.getText().toString()) >= goal+(sens*0.1)){
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Correção de glicemia");
            alert.setMessage("Sua glicemia está "+glycemia+". Sugiro que você aplique "+bolus+" unidades de insulina." +
                    " Posso registrar essa dosagem?");
            alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SQLiteDatabase db = helper.getWritableDatabase();
                    ContentValues insulin = new ContentValues();
                    insulin.put("date", dateValue.getText().toString());
                    insulin.put("time", timeValue.getText().toString());
                    insulin.put("value", bolus);
                    insulin.put("type", getResources().getString(R.string.database_type_bolus));

                    db.insert("glycemia", null, insulin);
                    db.close();
                }
            });
            alert.setNegativeButton("Não", null);
            alert.show();
        }

        writeValuesInfo();
        helper.close();
        db.close();
    }

    private void changeDate(int day, int month, int year){
        Calendar calendar = Calendar.getInstance();

        if (day == -1 || month == -1 || year == -1){
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            dateValue.setText(String.format("%02d/%02d/%04d", day, (month+1), year));
        } else {
            DatePickerFragment date = new DatePickerFragment();
            Bundle args =  new Bundle();
            args.putInt("year", calendar.get(Calendar.YEAR));
            args.putInt("month", calendar.get(Calendar.MONTH));
            args.putInt("day", calendar.get(Calendar.DAY_OF_MONTH));
            date.setArguments(args);

            date.setCallBack(ondate);
            date.show(getFragmentManager(), "Datepicker");
        }
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateValue.setText(String.format("%02d/%02d/%04d", dayOfMonth, (monthOfYear+1), year));
        }
    };

    private void changeTime(int hour, int minute){
        Calendar calendar = Calendar.getInstance();

        if (hour == -1 || minute == -1){
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);

            timeValue.setText(String.format("%02d:%02d", hour, minute));
        }else {
            TimePickerFragment time = new TimePickerFragment();
            Bundle args = new Bundle();
            args.putInt("hour", calendar.get(Calendar.HOUR_OF_DAY));
            args.putInt("minute", calendar.get(Calendar.MINUTE));
            time.setArguments(args);

            time.setCallBack(onTime);
            time.show(getFragmentManager(), "Timepicker");
        }
    }

    TimePickerDialog.OnTimeSetListener onTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            timeValue.setText(String.format("%02d:%02d", hourOfDay, minute));
        }
    };

    private void  writeValuesInfo(){
        db = helper.getReadableDatabase();
        int average=0;
        int counter = 0;

        String[] fields = {"value", "date","time", "type"};
        String where = "type = '"+getResources().getString(R.string.database_type_glycemia)+"'";
        Cursor cursor = db.query("glycemia", fields, where, null, null, null, "date DESC, time DESC");

        if (!(cursor.moveToFirst()) || cursor.getCount() == 0) {
            lastValue.setText("Nenhuma glicemia registrada");
            avgValue.setText("");

        } else {
            int valor = cursor.getInt(cursor.getColumnIndex("value"));
            String time = cursor.getString(cursor.getColumnIndex("time"));
            lastValue.setText("Última glicemia: " +valor+" as "+time);

            String[] fieldsAvg = {"value", "date"};
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Calendar calendar = Calendar.getInstance();

            for (int i = 0; i <= 6; i++) {
                calendar.setTime(new Date());
                calendar.add(Calendar.DAY_OF_YEAR, -i);
                String dateFactor = dateFormat.format(calendar.getTime());
                where = "date = '" + dateFactor + "' and type = '"+getResources().getString(R.string.database_type_glycemia)+"'";
                cursor = db.query("glycemia", fieldsAvg, where, null, null, null, null);
                cursor.moveToFirst();
                if(cursor.getCount() > 0){
                    for (int cont = 0; cont < cursor.getCount(); cont++){
                        average += cursor.getInt(cursor.getColumnIndex("value"));
                        counter ++;
                        cursor.moveToNext();
                    }
                }
            }

            average = average/counter;
            avgValue.setText("Média dos últimos 7 dias: " + average);
            value.setText("100");
        }
    }
}