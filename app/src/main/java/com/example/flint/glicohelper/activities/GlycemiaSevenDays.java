package com.example.flint.glicohelper.activities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.example.flint.glicohelper.R;
import com.example.flint.glicohelper.database.DatabaseHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class GlycemiaSevenDays extends AppCompatActivity{
    private DatabaseHelper helper;
    private String[] fields = {"time", "type", "value", "_id"};
    private int[] idViews = {R.id.glycemiaTime, R.id.glycemiaType, R.id.glycemiaValue};
    private SQLiteDatabase db;
    private ListView glycemiaList;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.glycemia_seven_days);

        list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.food_layout, list);

        glycemiaList = (ListView) findViewById(R.id.list_glycemia_seven_days);
        helper = new DatabaseHelper(this);
        db = helper.getReadableDatabase();

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy");

        for (int i = 0; i <= 6; i++){
            calendar.setTime(new Date());
            calendar.add(Calendar.DAY_OF_YEAR, -i);
            String dateFactor = dateFormat.format(calendar.getTime());
            String where = "date = '"+dateFactor+"'";
            Cursor cursor = db.query("glycemia", fields, where, null, null, null, "date desc, time asc");
            cursor.moveToFirst();

//            TextView dateItem = new TextView(this);
//            dateItem.setTypeface(null, Typeface.BOLD);
//            dateItem.setGravity(Gravity.CENTER_HORIZONTAL);
//            dateItem.setText(dateFactor);

            list.add("                        "+dateFactor);


            for(int cont = 0; cont < cursor.getCount(); cont++){

                String time = cursor.getString(cursor.getColumnIndex("time"));
                String type = cursor.getString(cursor.getColumnIndex("type"));
                double value = cursor.getDouble(cursor.getColumnIndex("value"));

                if(type.equals("Glicemia")){
                    String component = ""+time+" - "+type+"      "+value;
                    list.add(component);
                }

                if(type.equals("Refeição")){
                    String component = ""+time+" - "+type+"      "+value+" cho";
                    list.add(component);
                }

                if(type.equals("Bolus")){
                    String component = ""+time+" - "+type+"           "+value+"u";
                    list.add(component);
                }
                cursor.moveToNext();
            }
        }

        glycemiaList.setAdapter(adapter);
        db.close();
    }
}