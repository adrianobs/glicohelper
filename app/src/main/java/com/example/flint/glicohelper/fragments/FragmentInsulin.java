package com.example.flint.glicohelper.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.flint.glicohelper.R;
import com.example.flint.glicohelper.database.DatabaseHelper;

import java.util.Calendar;

public class FragmentInsulin extends Fragment {
    private Button dateInsulin, timeInsulin, sendInsulin;
    private EditText insulin;
    private TextView lastInsulin;
    private DatabaseHelper helper;
    private SQLiteDatabase db;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_insulin, container, false);

        dateInsulin = (Button) rootView.findViewById(R.id.button_dateInsulin);
        timeInsulin = (Button) rootView.findViewById(R.id.button_timeInsulin);
        sendInsulin = (Button) rootView.findViewById(R.id.button_sendInsulin);
        lastInsulin = (TextView) rootView.findViewById(R.id.textView_lastInsulin);
        insulin = (EditText) rootView.findViewById(R.id.editText_insulin);
        helper =  new DatabaseHelper(getContext());
        insulin.setText("0.5");

        changeDate(-1, -1, -1);
        changeTime(-1, -1);
        writeInsulinInfo();

        dateInsulin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDate(0, 0, 0);
            }
        });

        timeInsulin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTime(0, 0);
            }
        });

        sendInsulin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInsulin();
            }
        });

        return rootView;
    }

    private void sendInsulin(){
        db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("date", dateInsulin.getText().toString());
        values.put("time", timeInsulin.getText().toString());
        values.put("value", insulin.getText().toString());
        values.put("type", getResources().getString(R.string.database_type_bolus));

        long result = db.insert("glycemia", null, values);

        if(result != -1){
            Toast.makeText(getContext(), "Bolus registrado.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Bolus não registrado", Toast.LENGTH_SHORT).show();
        }

        writeInsulinInfo();
        helper.close();
        db.close();
    }

    private void changeDate(int day, int month, int year){
        Calendar calendar = Calendar.getInstance();

        if (day == -1 || month == -1 || year == -1){
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            dateInsulin.setText(String.format("%02d/%02d/%04d", day, (month+1), year));
        } else {
            DatePickerFragment date = new DatePickerFragment();
            Bundle args =  new Bundle();
            args.putInt("year", calendar.get(Calendar.YEAR));
            args.putInt("month", calendar.get(Calendar.MONTH));
            args.putInt("day", calendar.get(Calendar.DAY_OF_MONTH));
            date.setArguments(args);

            date.setCallBack(ondate);
            date.show(getFragmentManager(), "Datepicker");
        }
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateInsulin.setText(String.format("%02d/%02d/%04d", dayOfMonth, (monthOfYear+1), year));
        }
    };

    private void changeTime(int hour, int minute){
        Calendar calendar = Calendar.getInstance();

        if (hour == -1 || minute == -1){
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);

            timeInsulin.setText(String.format("%02d:%02d", hour, minute));
        }else {
            TimePickerFragment time = new TimePickerFragment();
            Bundle args = new Bundle();
            args.putInt("hour", calendar.get(Calendar.HOUR_OF_DAY));
            args.putInt("minute", calendar.get(Calendar.MINUTE));
            time.setArguments(args);

            time.setCallBack(onTime);
            time.show(getFragmentManager(), "Timepicker");
        }
    }

    TimePickerDialog.OnTimeSetListener onTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            timeInsulin.setText(String.format("%02d:%02d", hourOfDay, minute));
        }
    };

    private void  writeInsulinInfo(){
        db = helper.getReadableDatabase();
        String[] fields = {"value", "date","time", "type"};
        String where = "type = '"+getResources().getString(R.string.database_type_bolus)+"'";
        Cursor cursor = db.query("glycemia", fields, where, null, null, null, "date DESC, time DESC");

        if (!(cursor.moveToFirst()) || cursor.getCount() == 0) {
            lastInsulin.setText("Nenhum bolus registrado");

        } else {
            double valor = cursor.getDouble(cursor.getColumnIndex("value"));
            String time = cursor.getString(cursor.getColumnIndex("time"));
            lastInsulin.setText("Último bolus: " +valor+" as "+time);
        }
        insulin.setText("0.5");
        db.close();
    }
}