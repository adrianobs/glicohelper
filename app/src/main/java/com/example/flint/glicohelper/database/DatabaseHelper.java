package com.example.flint.glicohelper.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String BANCO_DADOS = "banco.db";
    public static int VERSAO = 1;

    public DatabaseHelper(Context context){
        super(context,BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE glycemia(" +
                "_id integer primary key autoincrement," +
                "value integer," +
                "date date," +
                "time date," +
                "type text);");

        db.execSQL("CREATE TABLE user(" +
                "_id integer primary key autoincrement," +
                "name text," +
                "age integer," +
                "sensibility integer," +
                "glycemiaGoal integer," +
                "bolusFactor integer);");

        db.execSQL("CREATE TABLE food(" +
                "_id integer primary key autoincrement," +
                "name text," +
                "cho int);");

        ContentValues values = new ContentValues();
        values.put("name", "Usuário");
        values.put("age", 20);
        values.put("sensibility", 50);
        values.put("glycemiaGoal", 100);
        values.put("bolusFactor", 8);

        db.insert("user", null, values);

        values = new ContentValues();
        values.put("name", "Arroz cozido - colher de sopa");
        values.put("cho", 5);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Abacaxi - fatia");
        values.put("cho", 10);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Aguardente – dose");
        values.put("cho", 20);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Alface - prato de sobremesa");
        values.put("cho", 1);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Almondega de carne - unidade");
        values.put("cho", 2);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Almondega de peru - unidade");
        values.put("cho", 0);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Ameixa - unidade");
        values.put("cho", 16);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Amora - unidade");
        values.put("cho", 8);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Feijao - colher de sopa");
        values.put("cho", 4);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Leite - 200ml");
        values.put("cho", 10);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Pao frances - unidade");
        values.put("cho", 10);
        db.insert("food", null, values);

        values = new ContentValues();
        values.put("name", "Rucula - pires");
        values.put("cho", 1);
        db.insert("food", null, values);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS glycemia");
        onCreate(db);
    }

    public void updateUser(SQLiteDatabase db, String name, int age, int sensibility, int glycemiaGoal, int bolusFactor){
        ContentValues values = new ContentValues();
        values.put("name", name);
        values.put("age", age);
        values.put("sensibility", sensibility);
        values.put("glycemiaGoal", glycemiaGoal);
        values.put("bolusFactor", bolusFactor);

        db.update("user", values, "_id = ?", new String[] {"1"});
    }
}