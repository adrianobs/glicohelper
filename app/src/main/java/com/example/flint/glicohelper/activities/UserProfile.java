package com.example.flint.glicohelper.activities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.flint.glicohelper.R;
import com.example.flint.glicohelper.database.DatabaseHelper;

public class UserProfile extends AppCompatActivity {

    private DatabaseHelper helper = new DatabaseHelper(this);
    private SQLiteDatabase db;
    private EditText textName, textAge, textSensibility, textGoal, textChoFactor;
    private Button confirm, cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        confirm = (Button) findViewById(R.id.button_user_update);
        cancel = (Button) findViewById(R.id.button_user_cancel);
        textName = (EditText) findViewById(R.id.editText_user_name);
        textAge = (EditText) findViewById(R.id.editText_user_age);
        textSensibility = (EditText) findViewById(R.id.editText_user_sensibility);
        textGoal = (EditText) findViewById(R.id.editText_user_goal);
        textChoFactor = (EditText) findViewById(R.id.editText_user_choFactor);

        setData();
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = textName.getText().toString();
                int age = Integer.parseInt(textAge.getText().toString());
                int sensibility = Integer.parseInt(textSensibility.getText().toString());
                int goal = Integer.parseInt(textGoal.getText().toString());
                int factor = Integer.parseInt(textChoFactor.getText().toString());

                helper.updateUser(db, name, age, sensibility, goal, factor);
                showMessage();
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setData(){
        db = helper.getReadableDatabase();
        String[] fields = {"name", "age", "sensibility", "glycemiaGoal", "bolusFactor"};
        Cursor cursor = db.query("user", fields, null, null, null, null, null);
        cursor.moveToFirst();

        String name = cursor.getString(cursor.getColumnIndex("name"));
        String age = cursor.getString(cursor.getColumnIndex("age"));
        int sensibility = cursor.getInt(cursor.getColumnIndex("sensibility"));
        int glycemiaGoal = cursor.getInt(cursor.getColumnIndex("glycemiaGoal"));
        int bolusFactor = cursor.getInt(cursor.getColumnIndex("bolusFactor"));

        textName.setText(name);
        textAge.setText(age);
        textSensibility.setText("" + sensibility);
        textGoal.setText("" + glycemiaGoal);
        textChoFactor.setText("" + bolusFactor);
    }

    private void showMessage(){
        Toast.makeText(this, "Os dados foram atualizados.", Toast.LENGTH_SHORT).show();
    }
}