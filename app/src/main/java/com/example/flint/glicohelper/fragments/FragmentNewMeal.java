package com.example.flint.glicohelper.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.flint.glicohelper.R;
import com.example.flint.glicohelper.database.DatabaseHelper;

import java.util.ArrayList;
import java.util.Calendar;

public class FragmentNewMeal extends Fragment {
    private Button dateMeal, timeMeal, sendMeal, addFood;
    private EditText cho, foodQuantity;
    private AutoCompleteTextView food;
    private DatabaseHelper helper;
    private SQLiteDatabase db;
    private ListView lv;
    private int totalCho = 0;
    private ArrayList<String> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_meal, container, false);

        dateMeal = (Button) rootView.findViewById(R.id.button_dateMeal);
        timeMeal = (Button) rootView.findViewById(R.id.button_timeMeal);
        sendMeal = (Button) rootView.findViewById(R.id.button_sendMeal);
        addFood = (Button) rootView.findViewById(R.id.button_add_food);
        cho = (EditText) rootView.findViewById(R.id.editText_cho);
        foodQuantity = (EditText) rootView.findViewById(R.id.editText_quantity_food);
        lv = (ListView) rootView.findViewById(R.id.list);
        food =(AutoCompleteTextView) rootView.findViewById(R.id.editText_add_food);
        helper = new DatabaseHelper(getContext());
        list = new ArrayList<>();

        foodQuantity.setText("1");
        cho.setText("0");

        //Criar a lista de alimentos a serem auto completados
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.food_layout, FOOD);
        food.setAdapter(adapter);
        //

        changeDate(-1, -1, -1);
        changeTime(-1, -1);

        dateMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDate(0, 0, 0);
            }
        });

        timeMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTime(0, 0);
            }
        });

        sendMeal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMeal();
            }
        });

        addFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createFoodList(food.getText().toString(), Integer.parseInt(foodQuantity.getText().toString()));
            }
        });
        return rootView;
    }

    private static final String[] FOOD = new String[]{
            "Arroz cozido - colher de sopa", "Abacaxi - fatia", "Aguardente – dose",
            "Alface - prato de sobremesa", "Almondega de carne - unidade", "Almondega de peru - unidade",
            "Ameixa - unidade", "Amora - unidade", "Feijao - colher de sopa", "Leite - 200ml",
            "Pao frances - unidade", "Rucula - pires"
    };

    private void createFoodList(String foodItem, int foodQuantity){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.food_layout, list);

        db = helper.getReadableDatabase();
        String[] fields = {"cho", "_id", "name"};
        String where = "name = '"+foodItem+"'";
        Cursor cursor = db.query("food", fields, where, null, null, null, null);
        cursor.moveToFirst();

        int cho = cursor.getInt(cursor.getColumnIndex("cho")) * foodQuantity;
        String mealItem = foodQuantity + " x  " + foodItem + ". Cho = " + cho+"gr";
        list.add(mealItem);
        lv.setAdapter(adapter);

        totalCho += cho;
        this.cho.setText(""+totalCho);
        food.setText("");
        this.foodQuantity.setText("1");
    }

    private void sendMeal(){
        db= helper.getReadableDatabase();

        String[] fields = {"bolusFactor"};
        Cursor cursor = db.query("user", fields, null, null, null, null, null);
        cursor.moveToFirst();

        int bolusFactor = cursor.getInt(cursor.getColumnIndex("bolusFactor"));
        int ch = Integer.parseInt(cho.getText().toString());
        final double bolus = (double) ch / bolusFactor;

        ContentValues values = new ContentValues();
        values.put("date", dateMeal.getText().toString());
        values.put("value", cho.getText().toString());
        values.put("time", timeMeal.getText().toString());
        values.put("type", getResources().getString(R.string.database_type_meal));

        long result = db.insert("glycemia", null, values);

        if(result != -1){
            Toast.makeText(getContext(), "Refeição registrada.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Refeição não registrada.", Toast.LENGTH_SHORT).show();
        }

        if(bolus >= 0.1){
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
            alert.setTitle("Correção de refeição");
            alert.setMessage("Você ingeriu " + ch + " carboidratos. Sugiro que você aplique " + bolus + " unidades de insulina." +
                    "Posso registrar essa dosagem?");
            alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SQLiteDatabase db = helper.getWritableDatabase();
                    ContentValues insulin = new ContentValues();
                    insulin.put("date", dateMeal.getText().toString());
                    insulin.put("time", timeMeal.getText().toString());
                    insulin.put("value", bolus);
                    insulin.put("type", getResources().getString(R.string.database_type_bolus));
                    db.insert("glycemia", null, insulin);
                    db.close();
                    food.setText("");
                    foodQuantity.setText("1");
                    totalCho = 0;
                    list.clear();
                    cho.setText("0");
                }
            });
            alert.setNegativeButton("Não", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    food.setText("");
                    foodQuantity.setText("1");
                    totalCho = 0;
                    list.clear();
                    cho.setText("0");
                }
            });
            alert.show();
        } else {
            food.setText("");
            foodQuantity.setText("1");
            totalCho = 0;
            list.clear();
            cho.setText("0");
        }
        helper.close();
        db.close();



    }

    private void changeDate(int day, int month, int year){
        Calendar calendar = Calendar.getInstance();

        if (day == -1 || month == -1 || year == -1){
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            dateMeal.setText(String.format("%02d/%02d/%04d", day, (month+1), year));
        } else {
            DatePickerFragment date = new DatePickerFragment();
            Bundle args =  new Bundle();
            args.putInt("year", calendar.get(Calendar.YEAR));
            args.putInt("month", calendar.get(Calendar.MONTH));
            args.putInt("day", calendar.get(Calendar.DAY_OF_MONTH));
            date.setArguments(args);

            date.setCallBack(ondate);
            date.show(getFragmentManager(), "Datepicker");
        }
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateMeal.setText(String.format("%02d/%02d/%04d", dayOfMonth, (monthOfYear+1), year));
        }
    };

    private void changeTime(int hour, int minute){
        Calendar calendar = Calendar.getInstance();

        if (hour == -1 || minute == -1){
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);

            timeMeal.setText(String.format("%02d:%02d", hour, minute));
        }else {
            TimePickerFragment time = new TimePickerFragment();
            Bundle args = new Bundle();
            args.putInt("hour", calendar.get(Calendar.HOUR_OF_DAY));
            args.putInt("minute", calendar.get(Calendar.MINUTE));
            time.setArguments(args);

            time.setCallBack(onTime);
            time.show(getFragmentManager(), "Timepicker");
        }
    }

    TimePickerDialog.OnTimeSetListener onTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            timeMeal.setText(String.format("%02d:%02d", hourOfDay, minute));
        }
    };
}